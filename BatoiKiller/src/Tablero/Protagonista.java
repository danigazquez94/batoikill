package Tablero;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Protagonista extends Personaje{
	
	static int x;
	static int y;
	static String ruta;
	static ImageIcon icon;
	static Image img;
	static Image otraimg;
	static ImageIcon iconoPersonaje;
	
	static int oro;
	static double velocidad;
	static double fuerza;
	static double sabiduria;
	static int energia;
	
	public Protagonista(int posX, int posY){
		
		super();
		
		// icono del personaje
		ruta = "yo.png";
		icon = new ImageIcon(ruta);
		img = icon.getImage(); //convertimos icon en una imagen
		otraimg = img.getScaledInstance(50,50,java.awt.Image.SCALE_SMOOTH); //creamos una imagen nueva d�ndole las dimensiones que queramos a la antigua
		iconoPersonaje = new ImageIcon(otraimg);
		
		fuerza = 1;
		velocidad = 1;
		oro = 0;
		energia = 100;
		sabiduria = 1;
		
		x = posX;
		y = posY;
		
	}


	public static ImageIcon getIconoPersonaje() {
		return iconoPersonaje;
	}

	public static void setIconoPersonaje(ImageIcon iconoPersonaje) {
		Protagonista.iconoPersonaje = iconoPersonaje;
	}

	public static int getOro() {
		return oro;
	}

	public static void setOro(int oro) {
		Protagonista.oro = oro;
	}

	public static double getVelocidad() {
		return velocidad;
	}

	public static void setVelocidad(double velocidad) {
		Protagonista.velocidad = velocidad;
	}

	public static double getFuerza() {
		return fuerza;
	}

	public static void setFuerza(double fuerza) {
		Protagonista.fuerza = fuerza;
	}

	public static double getSabiduria() {
		return sabiduria;
	}

	public static void setSabiduria(double sabiduria) {
		Protagonista.sabiduria = sabiduria;
	}

	public static int getEnergia() {
		return energia;
	}

	public static void setEnergia(int energia) {
		Protagonista.energia = energia;
	}

	public static String getRuta() {
		
		return Protagonista.ruta;
		
	}
	
	public static int getX() {
		return x;
	}

	public static void setX(int x) {
		Protagonista.x = x;
	}

	public static int getY() {
		return y;
	}

	public static void setY(int y) {
		Protagonista.y = y;
	}
	
	
}
