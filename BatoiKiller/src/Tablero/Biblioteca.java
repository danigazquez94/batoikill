package Tablero;

import java.awt.Color;

import javax.swing.JButton;

public class Biblioteca extends Edificacion {
	
	public Biblioteca() {
		
		super();
		edificio.setBackground(Color.YELLOW);
		
	}

	public JButton getBiblioteca() {
		return edificio;
	}
	
	public static void bonus() {
		
		Protagonista.setEnergia(Protagonista.getEnergia()+(int)((Math.round(Protagonista.getEnergia()*0.05))));
		Protagonista.setOro(Protagonista.getOro()+10);
		Protagonista.setSabiduria(Protagonista.getSabiduria()+Protagonista.getSabiduria()*0.25);
		
		if(Protagonista.getEnergia()>100) {
			
			Protagonista.setEnergia(100);
			
		}
		
		Tablero.getSabiduria().setText("SABIDURIA: "+Math.round(Protagonista.getSabiduria()*100d)/100d);
		Tablero.getOro().setText("ORO: "+Protagonista.getOro());
		Tablero.getEnergia().setText("ENERGIA: "+Protagonista.getEnergia());
		
	}
	
}
