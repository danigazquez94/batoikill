package Tablero;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ControladorTablero implements ActionListener {

    int movimientos;
    int ptsMovimiento;
    boolean playing;
    Icon iconoAnterior;
    String nombreAnterior;
    int antX;
    int antY;
    boolean ocupadaAnterior;
    static PersonajeGuardado otroPersonaje;

    public ControladorTablero(JPanel jpanel) {
       Casilla.panel = jpanel;
       movimientos = 0;
       ptsMovimiento = 0;
       playing = false;
       iconoAnterior = null;
       nombreAnterior = null;
       antX = 0;
       antY = 0;
       ocupadaAnterior = false;
       otroPersonaje = new PersonajeGuardado();

    }

    @Override
    public void actionPerformed(ActionEvent e) {
    	
        JButton boton = (JButton) e.getSource();
        boolean semaforo = true;
        Tablero.getAjusticiar().setEnabled(false);

        try {

            // BOTONES DE JUEGO
            // ---------------------------------------------------------
            // BOTON DE PLAY
            if (boton == Tablero.getPlay()) {

                movimientos = 0;
                ptsMovimiento = 0;
                ptsMovimiento = new Dado().getPtsMovimiento(); // llama a la clase dado y esta genera un numero  entre 1 y 6 y lo multiplica por la velocidad
                playing = true;
                Tablero.getAjusticiar().setEnabled(true);
                Tablero.getPlay().setEnabled(false);
                JOptionPane.showMessageDialog(Casilla.panel,
                        "Se ha lanzado el dado, puedes realizar " + ptsMovimiento + " movimientos", "TURNO DEL JUGADOR",
                        JOptionPane.INFORMATION_MESSAGE);

            } else if (playing == true && boton != Tablero.getPlay()) {

                if (movimientos < ptsMovimiento) {

                    semaforo = true;

                    Tablero.getAjusticiar().setEnabled(true);
                    Tablero.getPlay().setEnabled(false); // mientras no se haya completado la fase de movimiento no se
                    // puede volver a lanzar el dado

                    for (int i = 0; i < Casilla.botones.length && semaforo; i++) {

                        for (int j = 0; j < Casilla.botones[i].length && semaforo; j++) {

                            if (Casilla.botones[i][j].getName() != null && Casilla.botones[i][j].getName().equals(Protagonista.getRuta())) {

                            	System.out.println("getName->"+Casilla.botones[i][j].getName()+" i = " + i + ", j =" +j );
                                // ---------------------------------------------------------
                                // BOTONES DE MOVIMIENTO

                                if (boton == Tablero.getRight()) {  // movimiento derecha

                                    if (Casilla.botones[i][j + 1].getName() != null) {
                                        if (!Casilla.botones[i][j + 1].getName().equals(Protagonista.getRuta())) { //SI ES OTRO PERSONAJE CAPTURALO
                                            otroPersonaje = new PersonajeGuardado();
                                            otroPersonaje.setIconoAnterior(Casilla.botones[i][j + 1].getIcon());
                                            otroPersonaje.setNombreAnterior(Casilla.botones[i][j + 1].getName());
                                            otroPersonaje.setPosicionX(i);
                                            otroPersonaje.setPosicionY(j + 1);
                                        }
                                    }

                                    Casilla.botones[i][j + 1].setIcon(Protagonista.getIconoPersonaje());
                                    Casilla.botones[i][j + 1].setName(Protagonista.getRuta());

                                    Casilla.botones[i][j].setName(null);
                                    Casilla.botones[i][j].setIcon(null);

                                    if (ocupadaAnterior) {
                                        if (otroPersonaje.posicionX != i || otroPersonaje.posicionY != j + 1) {
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setName(otroPersonaje.getNombreAnterior());
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setIcon(otroPersonaje.getIconoAnterior());
                                            otroPersonaje.setNombreAnterior(null);
                                        }
                                    }

                                    movimientos++;

                                    semaforo = false;

                                } else if (boton == Tablero.getLeft()) { // movimiento izquierda

                                    if (Casilla.botones[i][j - 1].getName() != null) {
                                        if (!Casilla.botones[i][j - 1].getName().equals(Protagonista.getRuta())) { //SI ES OTRO PERSONAJE CAPTURALO
                                            otroPersonaje = new PersonajeGuardado();
                                            otroPersonaje.setIconoAnterior(Casilla.botones[i][j - 1].getIcon());
                                            otroPersonaje.setNombreAnterior(Casilla.botones[i][j - 1].getName());
                                            otroPersonaje.setPosicionX(i);
                                            otroPersonaje.setPosicionY(j - 1);
                                        }
                                    }
                                    
                                    Casilla.botones[i][j - 1].setIcon(Protagonista.getIconoPersonaje());
                                    Casilla.botones[i][j - 1].setName(Protagonista.getRuta());

                                    Casilla.botones[i][j].setName(null);
                                    Casilla.botones[i][j].setIcon(null);
                                    
                                     if (ocupadaAnterior) {
                                        if (otroPersonaje.posicionX != i || otroPersonaje.posicionY != j - 1) {
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setName(otroPersonaje.getNombreAnterior());
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setIcon(otroPersonaje.getIconoAnterior());
                                            otroPersonaje.setNombreAnterior(null);
                                        }
                                    }

                                    movimientos++;

                                    semaforo = false;

                                } else if (boton == Tablero.getTop()) { // movimiento arriba

                                    if (Casilla.botones[i - 1][j].getName() != null) {
                                        if (!Casilla.botones[i - 1][j].getName().equals(Protagonista.getRuta())) { //SI ES OTRO PERSONAJE CAPTURALO
                                            otroPersonaje = new PersonajeGuardado();
                                            otroPersonaje.setIconoAnterior(Casilla.botones[i - 1][j].getIcon());
                                            otroPersonaje.setNombreAnterior(Casilla.botones[i - 1][j].getName());
                                            otroPersonaje.setPosicionX(i - 1);
                                            otroPersonaje.setPosicionY(j);
                                        }
                                    }
                                    Casilla.botones[i - 1][j].setIcon(Protagonista.getIconoPersonaje());
                                    Casilla.botones[i - 1][j].setName(Protagonista.getRuta());

                                    Casilla.botones[i][j].setName(null);
                                    Casilla.botones[i][j].setIcon(null);

                                     if (ocupadaAnterior) {
                                    	 
                                        if (otroPersonaje.posicionX != i - 1 || otroPersonaje.posicionY != j) {
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setName(otroPersonaje.getNombreAnterior());
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setIcon(otroPersonaje.getIconoAnterior());
                                            otroPersonaje.setNombreAnterior(null);
                                        }
                                        
                                    }
                                    
                                    movimientos++;

                                    semaforo = false;

                                } else if (boton == Tablero.getBot()) { // movimiento abajo
                                  
                                	if (Casilla.botones[i + 1][j].getName() != null) {
                                        if (!Casilla.botones[i + 1][j].getName().equals(Protagonista.getRuta())) { //SI ES OTRO PERSONAJE CAPTURALO
                                            otroPersonaje = new PersonajeGuardado();
                                            otroPersonaje.setIconoAnterior(Casilla.botones[i + 1][j].getIcon());
                                            otroPersonaje.setNombreAnterior(Casilla.botones[i + 1][j].getName());
                                            otroPersonaje.setPosicionX(i + 1);
                                            otroPersonaje.setPosicionY(j);
                                        }
                                    }
                                	
                                    Casilla.botones[i + 1][j].setIcon(Protagonista.getIconoPersonaje());
                                    Casilla.botones[i + 1][j].setName(Protagonista.getRuta());

                                    Casilla.botones[i][j].setName(null);
                                    Casilla.botones[i][j].setIcon(null);

                                    
                                     if (ocupadaAnterior) {
                                        if (otroPersonaje.posicionX != i +1  || otroPersonaje.posicionY != j) {
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setName(otroPersonaje.getNombreAnterior());
                                            Casilla.botones[otroPersonaje.posicionX][otroPersonaje.posicionY].setIcon(otroPersonaje.getIconoAnterior());
                                            otroPersonaje.setNombreAnterior(null);
                                        }
                                    }
                                    
                                    movimientos++;

                                    semaforo = false;

                                } else if (boton == Tablero.getAjusticiar()) {

                                    // acciones de reclamo de recompensa o de combate
                                	
                                    if (JOptionPane.showConfirmDialog(null, "Acci�n de reclamo de recompensa o de combate ", "AJUSTICIAR CASILLA",
                                    		JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                                    	// yes option: lanza el evento pero pierdes los pnts de movimiento
                                    	new Evento();
                                    	
                                    	Tablero.getAjusticiar().setEnabled(false);
                                    	
                                    	movimientos = ptsMovimiento;
                                    	
                                    } else {
                                        // no option
                                    }
                                    
                                    Tablero.getAjusticiar().setEnabled(false);

                                }

                            }
                        
                        }                    
                        
                       
                        
                        if(otroPersonaje != null){
                            ocupadaAnterior = true;
                        }
                        
                        
                        
                    }

                }

                if (movimientos == ptsMovimiento) {

                    Tablero.getPlay().setEnabled(true);
                  
                    playing = false;

                }

            } else if (playing == false && boton != Tablero.getPlay() && boton != Tablero.getAjusticiar() ) {

                JOptionPane.showMessageDialog(Casilla.panel,
                        "No tienes puntos de movimiento debes darle al play para conseguirlos ", "SIN MOVIMIENTOS",
                        JOptionPane.INFORMATION_MESSAGE);

            } else if (boton == Tablero.getAjusticiar()) {

                // acciones de reclamo de recompensa o de combate
            	
                if (JOptionPane.showConfirmDialog(null, "Acci�n de reclamo de recompensa o de combate ", "AJUSTICIAR CASILLA",
                		JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                	// yes option: lanza el evento pero pierdes los pnts de movimiento
                	new Evento();
                	movimientos = ptsMovimiento;
                	
                } else {
                    // no option
                }
                
                Tablero.getAjusticiar().setEnabled(false);

            }

        } catch (Exception e1) {

             JOptionPane.showMessageDialog(Casilla.panel, "Error " + e1.getMessage());
             
        }
    }

    

}
