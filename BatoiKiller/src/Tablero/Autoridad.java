package Tablero;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Autoridad extends Personaje{

	static int x;
	static int y;
	static String ruta;
	static ImageIcon icon;
	static Image img;
	static Image otraimg;
	static ImageIcon iconoPersonaje;
	
	static int oro;
	static double velocidad;
	static double fuerza;
	static double sabiduria;
	static int energia;
	
	public Autoridad() {
		super();

		// icono del personaje
		ruta = "autoridad.png";
		icon = new ImageIcon(ruta);
		img = icon.getImage(); // convertimos icon en una imagen
		otraimg = img.getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH); // creamos una imagen nueva d�ndole las
																				// dimensiones que queramos a la antigua
		iconoPersonaje = new ImageIcon(otraimg);

		fuerza = 1.5;
		velocidad = 2;
		oro = 30;
		energia = 50;
		sabiduria = 1.5;
	}
	
	public Autoridad(int posX, int posY){
		
		super();
		
		// icono del personaje
		ruta = "autoridad.png";
		icon = new ImageIcon(ruta);
		img = icon.getImage(); //convertimos icon en una imagen
		otraimg = img.getScaledInstance(50,50,java.awt.Image.SCALE_SMOOTH); //creamos una imagen nueva d�ndole las dimensiones que queramos a la antigua
		iconoPersonaje = new ImageIcon(otraimg);
		
		fuerza = 1.5;
		velocidad = 1;
		oro = 50;
		energia = 100;
		sabiduria = 1;
		
		x = posX;
		y = posY;
		
	}
	
	public static ImageIcon getIconoPersonaje() {
		return iconoPersonaje;
	}

	public static void setIconoPersonaje(ImageIcon iconoPersonaje) {
		Autoridad.iconoPersonaje = iconoPersonaje;
	}

	public int getOro() {
		return oro;
	}

	public static void setOro(int oro) {
		Autoridad.oro = oro;
	}

	public static double getVelocidad() {
		return velocidad;
	}

	public static void setVelocidad(double velocidad) {
		Autoridad.velocidad = velocidad;
	}

	public static double getFuerza() {
		return fuerza;
	}

	public static void setFuerza(double fuerza) {
		Autoridad.fuerza = fuerza;
	}

	public static double getSabiduria() {
		return sabiduria;
	}

	public static void setSabiduria(double sabiduria) {
		Autoridad.sabiduria = sabiduria;
	}

	public int getEnergia() {
		return energia;
	}

	public void setEnergia(int energia) {
		Autoridad.energia = energia;
	}
	
public static String getRuta() {
		
		return ruta;
	}

public static int getX() {
	return x;
}

public static void setX(int x) {
	Autoridad.x = x;
}

public static int getY() {
	return y;
}

public static void setY(int y) {
	Autoridad.y = y;
}
}

