package Tablero;

import java.awt.Color;

import javax.swing.JButton;

public class Calle extends Edificacion{
	
	public Calle() {
		
		super();
		edificio.setBackground(Color.DARK_GRAY);
		
	}

	public static JButton getCalle() {
		return edificio;
	}
	
	public static void bonus() {
		
		Protagonista.setFuerza(Protagonista.getFuerza()+Protagonista.getFuerza()*0.05);
		Protagonista.setVelocidad(Protagonista.getVelocidad()+Protagonista.getVelocidad()*0.05);
		Protagonista.setOro(Protagonista.getOro()+5);
		Protagonista.setEnergia(Protagonista.getEnergia()+(int)((Math.round(Protagonista.getEnergia()*0.05))));
		
		if(Protagonista.getEnergia()>100) {
			
			Protagonista.setEnergia(100);
			
		}
		
		Tablero.getFuerza().setText("FUERZA: "+Math.round(Protagonista.getFuerza()*100d)/100d);
		Tablero.getOro().setText("ORO: "+Protagonista.getOro());
		Tablero.getVelocidad().setText("VELOCIDAD: "+Math.round(Protagonista.getVelocidad()*100d)/100d);
		Tablero.getEnergia().setText("ENERGIA: "+Protagonista.getEnergia());
		
	}
	
}
