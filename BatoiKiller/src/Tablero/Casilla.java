package Tablero;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Casilla extends JPanel {
	static JPanel panel;
	static JButton[][] botones;
	//ArrayList<Edificacion> edificios = new ArrayList<Edificacion>();
	int hospital;
	int biblioteca;
	int descampado;
	int gimnasio;

	public Casilla(int capacidad) {

		botones = new JButton[capacidad][capacidad];
		

		// SE CREA UN OYENTE DE ACCIONES Y SE LE PASA EL PANEL COMO ARGUMENTO
		ControladorCasilla oyenteAcciones = new ControladorCasilla(this);
		
		do {
			
			this.removeAll();
			
			hospital = 0;
			biblioteca = 0;
			descampado = 0;
			gimnasio = 0;
			
		for (int i = 0; i < botones.length; i++) {

			for (int j = 0; j < botones[i].length; j++) {

				// SE CREAN LOS BOTONES DEPENDIENDO DE SI DEBE SER TIPO CALLE O EDIFICIO
				if ((i % 2 == 0) || (j % 2 != 0)) {
					
					botones[i][j]= new Calle().getCalle();
					
					
				} else {
					
					botones[i][j] = tipoEdificio();
				
				}

				//botones[i][j].setText(""+i+""+j);
				botones[i][j].addActionListener(oyenteAcciones);
				botones[i][j].setBorder(null);
				botones[i][j].setPreferredSize(new Dimension(50, 50));
				add(botones[i][j]);

			}
		}
			
		} while (biblioteca < 5 || hospital < 5 || descampado < 5 || gimnasio < 5);
			
		setLayout(new GridLayout(capacidad, capacidad));
		
		
	}

	public JButton tipoEdificio() {
		
		JButton edificio = null;
		
		int rand = (int) (Math.random() * 4);


		switch (rand) {

		case 0:
			
			edificio = new Hospital().getHospital(); // hospital
			hospital++;
			break;

		case 1:
			
			edificio = new Gimnasio().getGimnasio(); // gimnasio
			gimnasio++;
			break;

		case 2:
			
			edificio = new Descampado().getDescampado(); // descampado
			descampado++;
			break;

		case 3:
			
			edificio = new Biblioteca().getBiblioteca(); // biblioteca
			biblioteca++;
			break;

		default:
			break;

		}
		
		return edificio;

	}

}
