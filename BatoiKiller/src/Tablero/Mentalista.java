package Tablero;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Mentalista extends Personaje{
	
	static int x;
	static int y;
	static String ruta;
	static ImageIcon icon;
	static Image img;
	static Image otraimg;
	static ImageIcon iconoPersonaje;
	
	static int oro;
	static double velocidad;
	static double fuerza;
	static double sabiduria;
	static int energia;
	
	public Mentalista() {
		super();
		
		// icono del personaje
		ruta = "mentalista.png";
		icon = new ImageIcon(ruta);
		img = icon.getImage(); //convertimos icon en una imagen
		otraimg = img.getScaledInstance(50,50,java.awt.Image.SCALE_SMOOTH); //creamos una imagen nueva d�ndole las dimensiones que queramos a la antigua
		iconoPersonaje = new ImageIcon(otraimg);
		
		
		fuerza = 1;
		velocidad = 2;
		oro = 30;
		energia = 50;
		sabiduria = 2;
	}
	
public Mentalista(int posX, int posY){
		
		super();
		
		// icono del personaje
		ruta = "mentalista.png";
		icon = new ImageIcon(ruta);
		img = icon.getImage(); //convertimos icon en una imagen
		otraimg = img.getScaledInstance(50,50,java.awt.Image.SCALE_SMOOTH); //creamos una imagen nueva d�ndole las dimensiones que queramos a la antigua
		iconoPersonaje = new ImageIcon(otraimg);
		
		
		fuerza = 1;
		velocidad = 2;
		oro = 30;
		energia = 50;
		sabiduria = 2;
		
		x = posX;
		y = posY;
		
	}

public static ImageIcon getIconoPersonaje() {
	return iconoPersonaje;
}

public static void setIconoPersonaje(ImageIcon iconoPersonaje) {
	Mentalista.iconoPersonaje = iconoPersonaje;
}

public int getOro() {
	return oro;
}

public static void setOro(int oro) {
	Mentalista.oro = oro;
}

public static double getVelocidad() {
	return velocidad;
}

public static void setVelocidad(double velocidad) {
	Mentalista.velocidad = velocidad;
}

public static double getFuerza() {
	return fuerza;
}

public static void setFuerza(double fuerza) {
	Mentalista.fuerza = fuerza;
}

public double getSabiduria() {
	return sabiduria;
}

public static void setSabiduria(double sabiduria) {
	Mentalista.sabiduria = sabiduria;
}

public int getEnergia() {
	return energia;
}

public void setEnergia(int energia) {
	Mentalista.energia = energia;
}

public static String getRuta() {
	
	return ruta;
}

public static int getX() {
	return x;
}

public static void setX(int x) {
	Mentalista.x = x;
}

public static int getY() {
	return y;
}

public static void setY(int y) {
	Mentalista.y = y;
}


	
}
