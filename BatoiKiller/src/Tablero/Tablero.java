package Tablero;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.*;

public class Tablero extends JPanel {

	// casillas del tablero
	
	int nEnemigos; // los enemigos segun la dificultad
	static int enemigosRestantes; // enemigos que quedan segun avanza el juego
	
	static Protagonista prota;
	static ArrayList <Personaje> personajes;

	// imagen de fondo
	private Image background;

	// botones de movimiento
	private static JButton top;
	private static JButton bot;
	private static JButton left;
	private static JButton right;
	private static JButton ajusticiar;
	private static JButton play;

	// imagenes de iconos redimensionadas

	static ImageIcon iconL = new ImageIcon("arrowL.jpg");
	static Image imgL = iconL.getImage(); // convertimos icon en una imagen
	static Image otraimgL = imgL.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH); // creamos una imagen nueva
																							// d�ndole las dimensiones
																							// que queramos a la antigua
	static ImageIcon otroiconL = new ImageIcon(otraimgL);

	static ImageIcon iconR = new ImageIcon("arrowR.jpg");
	static Image imgR = iconR.getImage(); // convertimos icon en una imagen
	static Image otraimgR = imgR.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH); // creamos una imagen nueva
																							// d�ndole las dimensiones
																							// que queramos a la antigua
	static ImageIcon otroiconR = new ImageIcon(otraimgR);

	static ImageIcon iconT = new ImageIcon("arrowT.jpg");
	static Image imgT = iconT.getImage(); // convertimos icon en una imagen
	static Image otraimgT = imgT.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH); // creamos una imagen nueva
																							// d�ndole las dimensiones
																							// que queramos a la antigua
	static ImageIcon otroiconT = new ImageIcon(otraimgT);

	static ImageIcon iconB = new ImageIcon("arrowB.jpg");
	static Image imgB = iconB.getImage(); // convertimos icon en una imagen
	static Image otraimgB = imgB.getScaledInstance(60, 60, java.awt.Image.SCALE_SMOOTH); // creamos una imagen nueva
																							// d�ndole las dimensiones
																							// que queramos a la antigua
	static ImageIcon otroiconB = new ImageIcon(otraimgB);

	// labels
	private JLabel dimension;
	private JLabel Estadisticas;
	private static JLabel fuerza;
	private static JLabel velocidad;
	private static JLabel oro;
	private static JLabel sabiduria;
	private static JLabel energia;
	
	private static String nombre;
	
	public Tablero() {

		ControladorTablero oyenteAccionesControles = new ControladorTablero(this);
		Casilla casillas = new Casilla(10);
		
		setNombre(PantallaPrincipal.txtNombre.getText()); // nombre del jugador
		prota = new Protagonista(0, 0);
		
		// instanciar componentes
		top = new JButton();
		top.addActionListener(oyenteAccionesControles);
		bot = new JButton();
		bot.addActionListener(oyenteAccionesControles);
		left = new JButton();
		left.addActionListener(oyenteAccionesControles);
		right = new JButton();
		right.addActionListener(oyenteAccionesControles);
		play = new JButton("Play");
		play.addActionListener(oyenteAccionesControles);
		ajusticiar = new JButton("Ajusticiar");
		ajusticiar.addActionListener(oyenteAccionesControles);
		ajusticiar.setEnabled(false); // por defecto el boton de combate/recompensa est� deshabilitado hasta que se
										// lance el dado

		dimension = new JLabel("10x10");
		Estadisticas = new JLabel(getNombre());
		energia = new JLabel("ENERGIA: " + Protagonista.getEnergia());
		fuerza = new JLabel("FUERZA: " + Protagonista.getFuerza());
		velocidad = new JLabel("VELOCIDAD: " + Protagonista.getVelocidad());
		oro = new JLabel("ORO: " + Protagonista.getOro());
		sabiduria = new JLabel("SABIDURIA: " + Protagonista.getSabiduria());

		// distribuci�n de los componentes
		this.setLayout(null);

		// a�adir componentes
		this.add(casillas);
		casillas.setBounds(0, 20, 500, 500);

		this.add(dimension);
		dimension.setBounds(230, -15, 50, 50);
		dimension.setForeground(Color.WHITE);

		this.add(Estadisticas);
		Estadisticas.setBounds(550, 10, 200, 50);
		Estadisticas.setFont(new Font("Verdana", Font.BOLD, 20));
		Estadisticas.setForeground(Color.RED);

		this.add(energia);
		energia.setBounds(550, 50, 200, 50);
		energia.setFont(new Font("Verdana", Font.BOLD, 15));

		if (Protagonista.getEnergia() > 70) {

			energia.setForeground(Color.GREEN);

		} else if (Protagonista.getEnergia() < 70 || Protagonista.getEnergia() > 30) {

			energia.setForeground(Color.YELLOW);

		} else if (Protagonista.getEnergia() < 30) {

			energia.setForeground(Color.RED);

		}

		this.add(fuerza);
		fuerza.setBounds(550, 70, 200, 50);
		fuerza.setFont(new Font("Verdana", Font.BOLD, 15));
		fuerza.setForeground(Color.YELLOW);

		this.add(velocidad);
		velocidad.setBounds(550, 90, 200, 50);
		velocidad.setFont(new Font("Verdana", Font.BOLD, 15));
		velocidad.setForeground(Color.YELLOW);

		this.add(oro);
		oro.setBounds(550, 110, 200, 50);
		oro.setFont(new Font("Verdana", Font.BOLD, 15));
		oro.setForeground(Color.YELLOW);

		this.add(sabiduria);
		sabiduria.setBounds(550, 130, 200, 50);
		sabiduria.setFont(new Font("Verdana", Font.BOLD, 15));
		sabiduria.setForeground(Color.YELLOW);

		top.setBounds(620, 250, 60, 60);
		this.add(top);
		top.setIcon(otroiconT);

		bot.setBounds(620, 380, 60, 60);
		this.add(bot);
		bot.setIcon(otroiconB);

		left.setBounds(555, 315, 60, 60);
		this.add(left);
		left.setIcon(otroiconL);

		right.setBounds(685, 315, 60, 60);
		this.add(right);
		right.setIcon(otroiconR);

		play.setBounds(545, 470, 100, 50);
		this.add(play);

		ajusticiar.setBounds(655, 470, 100, 50);
		this.add(ajusticiar);

		// COLOCAR PERSONAJES
		colocarPersonajes();

		this.setBackground("fondo-azul.jpg");
		this.setVisible(true);

	}

	// Metodo que es llamado automaticamente por la maquina virtual Java cada vez
	// que repinta
	public void paintComponent(Graphics g) {

		/*
		 * Obtenemos el tama�o del panel para hacer que se ajuste a este cada vez que
		 * redimensionemos la ventana y se lo pasamos al drawImage
		 */
		int width = this.getSize().width;
		int height = this.getSize().height;

		// Mandamos que pinte la imagen en el panel
		if (this.background != null) {
			g.drawImage(this.background, 0, 0, width, height, null);
		}

		super.paintComponent(g);
	}

	// Metodo donde le pasaremos la direcci�n de la imagen a cargar.
	public void setBackground(String imagePath) {

		// Construimos la imagen y se la asignamos al atributo background.
		this.setOpaque(false);
		this.background = new ImageIcon(imagePath).getImage();
		repaint();

	}

	public void colocarPersonajes() {

		nEnemigos = 0;
		int contadorEnemigos = 0;
		int cCiudadano = 0;
		int cAutoridad = 0;
		int cMentalista = 0;
		int cDelincuente = 0;
		String dificultad = "";
		
		dificultad = PantallaPrincipal.cmbLista.getSelectedItem().toString();
		
		if(dificultad.equals("F�cil")) {
			
			nEnemigos = 4;
			
		} else if(dificultad.equals("Intermedio")) {
			
			nEnemigos = 12;
			
		} else if(dificultad.equals("Dif�cil")) {
		
			nEnemigos = 24;
			
		}
		
		enemigosRestantes = nEnemigos;
		
		personajes = new ArrayList<Personaje>();

		// colocamos al protagonista
		Casilla.botones[0][0].setIcon(Protagonista.getIconoPersonaje());
		Casilla.botones[0][0].setName(Protagonista.getRuta());

		/*
		 * Mientras el numero total de enemigos que debe haber (en proporcion a la dificultad seleccionada)
		 * sea mayor al numero de enemicos colocados (contadorEnemigos) saca unas coordenadas aleatorias X e Y
		 * si el numero de enemigos de ese tipo es menor que el total de enemigos de ese tipo, y las coordenadas 
		 * no estan ocupadas, coloca un enemigo de ese  tipo en las coordenadas y suma 1 al tipo de enemigo que se ha colocado.  
		*/
		
		while (nEnemigos > contadorEnemigos) {

			int coorX = posicionAleatoria(); // numero aleatorio para la posicion X de personaje
			int coorY = posicionAleatoria(); // numero aleatorio para la posicion Y de personaje

			if (cCiudadano < nEnemigos/4 && Casilla.botones[coorX][coorY].getIcon()==null) {
				
				personajes.add(new Ciudadano(coorX,coorY));
				Casilla.botones[coorX][coorY].setIcon(Ciudadano.getIconoPersonaje());
				Casilla.botones[coorX][coorY].setName(Ciudadano.getRuta());
				cCiudadano++;

			} else if (cAutoridad < nEnemigos/4 && Casilla.botones[coorX][coorY].getIcon()==null) {
				
				personajes.add(new Autoridad(coorX,coorY));
				Casilla.botones[coorX][coorY].setIcon(Autoridad.getIconoPersonaje());
				Casilla.botones[coorX][coorY].setName(Autoridad.getRuta());
				cAutoridad++;

			} else if (cMentalista < nEnemigos/4 && Casilla.botones[coorX][coorY].getIcon()==null) {
				
				personajes.add(new Mentalista(coorX,coorY));
				Casilla.botones[coorX][coorY].setIcon(Mentalista.getIconoPersonaje());
				Casilla.botones[coorX][coorY].setName(Mentalista.getRuta());
				cMentalista++;

			} else if (cDelincuente < nEnemigos/4 && Casilla.botones[coorX][coorY].getIcon()==null) {

				personajes.add(new Delincuente(coorX,coorY));
				Casilla.botones[coorX][coorY].setIcon(Delincuente.getIconoPersonaje());
				Casilla.botones[coorX][coorY].setName(Delincuente.getRuta());
				cDelincuente++;

			}

			contadorEnemigos = cCiudadano + cAutoridad + cMentalista + cDelincuente;
		}

	}

	// numero aleatorio para posicion de los personajes
	public int posicionAleatoria() {

		int rand = 0;
		rand = (int) (Math.random() * 10);

		return rand;

	}
	
	public static JLabel getFuerza() {
		return fuerza;
	}

	public static void setFuerza(JLabel fuerza) {
		Tablero.fuerza = fuerza;
	}

	public static JLabel getVelocidad() {
		return velocidad;
	}

	public static void setVelocidad(JLabel velocidad) {
		Tablero.velocidad = velocidad;
	}

	public static JLabel getOro() {
		return oro;
	}

	public static void setOro(JLabel oro) {
		Tablero.oro = oro;
	}

	public static JLabel getSabiduria() {
		return sabiduria;
	}

	public static void setSabiduria(JLabel sabiduria) {
		Tablero.sabiduria = sabiduria;
	}
	
	public static JLabel getEnergia() {
		return energia;
	}

	public static void setEnergia(JLabel energia) {
		Tablero.energia = energia;
	}

	public static JButton getTop() {
		return top;
	}

	public void setTop(JButton top) {
		this.top = top;
	}

	public static JButton getBot() {
		return bot;
	}

	public void setBot(JButton bot) {
		this.bot = bot;
	}

	public static JButton getLeft() {
		return left;
	}

	public void setLeft(JButton left) {
		this.left = left;
	}

	public static JButton getRight() {
		return right;
	}

	public void setRight(JButton right) {
		this.right = right;
	}

	public static JButton getPlay() {
		return play;
	}

	public void setPlay(JButton play) {
		this.play = play;
	}

	public static JButton getAjusticiar() {
		return ajusticiar;
	}

	public static void setAjusticiar(JButton ajusticiar) {
		Tablero.ajusticiar = ajusticiar;
	}

	public static String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public static Protagonista getProta() {
		return prota;
	}

	public static void setProta(Protagonista prota) {
		Tablero.prota = prota;
	}

	public static ArrayList<Personaje> getPersonajes() {
		return personajes;
	}

	public static void setPersonajes(ArrayList<Personaje> personajes) {
		Tablero.personajes = personajes;
	}

}