package Tablero;

import java.awt.Color;

import javax.swing.JButton;

public class Gimnasio extends Edificacion {

	protected static JButton gimnasio;
	
	public Gimnasio() {
		
		super();
		gimnasio = new JButton();
		gimnasio.setBackground(Color.RED);
		
	}

	public JButton getGimnasio() {
		return gimnasio;
	}

	public void setGimnasio(JButton gimnasio) {
		this.gimnasio = gimnasio;
	}

	public static void bonus() {
		
		Protagonista.setFuerza(Protagonista.getFuerza()+Protagonista.getFuerza()*0.2);
		Protagonista.setVelocidad(Protagonista.getVelocidad()+Protagonista.getVelocidad()*0.1);
		Protagonista.setOro(Protagonista.getOro()+10);
		
		Tablero.getFuerza().setText("FUERZA: "+Math.round(Protagonista.getFuerza()*100d)/100d);
		Tablero.getOro().setText("ORO: "+Protagonista.getOro());
		Tablero.getVelocidad().setText("VELOCIDAD: "+Math.round(Protagonista.getVelocidad()*100d)/100d);
		
	}
	
	
	
}
