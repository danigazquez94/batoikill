package Tablero;

public class Dado {

	static int dado;
	int ptsMovimiento;
	
	public Dado() {
	
	dado = 0;
	ptsMovimiento = 0;

	while (dado == 0) {

		dado = (int) (Math.random() * 7);

	}
	
	ptsMovimiento = (int) (dado * Protagonista.getVelocidad()); 
	
	}

	public static int getDado() {
		return dado;
	}

	public void setDado(int dado) {
		this.dado = dado;
	}

	public int getPtsMovimiento() {
		return ptsMovimiento;
	}

	public void setPtsMovimiento(int ptsMovimiento) {
		this.ptsMovimiento = ptsMovimiento;
	}
	
	
	
}
