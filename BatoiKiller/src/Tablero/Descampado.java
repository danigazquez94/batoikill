package Tablero;

import java.awt.Color;

import javax.swing.JButton;

public class Descampado extends Edificacion{
	
	public Descampado() {
		
		super();
		edificio.setBackground(Color.BLACK);
		
	}

	public JButton getDescampado() {
		return edificio;
	}

	public static void bonus() {
		
		Protagonista.setFuerza(Protagonista.getFuerza()+Protagonista.getFuerza()*0.1);
		Protagonista.setVelocidad(Protagonista.getVelocidad()+Protagonista.getVelocidad()*0.1);
		Protagonista.setSabiduria(Protagonista.getSabiduria()+Protagonista.getSabiduria()*0.1);
		Protagonista.setEnergia(Protagonista.getEnergia()+(int)((Math.round(Protagonista.getEnergia()*0.1))));
		
		if(Protagonista.getEnergia()>100) {
			
			Protagonista.setEnergia(100);
			
		}
		
		Tablero.getFuerza().setText("FUERZA: "+Math.round(Protagonista.getFuerza()*100d)/100d);
		Tablero.getSabiduria().setText("SABIDURIA: "+Math.round(Protagonista.getSabiduria()*100d)/100d);
		Tablero.getVelocidad().setText("VELOCIDAD: "+Math.round(Protagonista.getVelocidad()*100d)/100d);
		Tablero.getEnergia().setText("ENERGIA: "+Protagonista.getEnergia());
		
	}
	
}
