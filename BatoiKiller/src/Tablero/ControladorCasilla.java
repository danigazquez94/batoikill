package Tablero;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ControladorCasilla implements ActionListener {
	
	public ControladorCasilla(JPanel jpanel) {
		Casilla.panel = jpanel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		// Se obtiene el color del boton pulsado
		JButton boton = (JButton) e.getSource();
		Color color = boton.getBackground();
		String edificio = "";

		if (color == Color.WHITE) {
			
			edificio = "Hospital";

		} else if (color == Color.RED) {
			
			edificio = "Gimnasio";

		} else if (color == Color.YELLOW) {

			edificio = "Biblioteca";
			
		} else if (color == Color.BLACK) {
			
			edificio = "Descampado";

		} else {
			
			edificio = "Calle";
			
		}
		JOptionPane.showMessageDialog(Casilla.panel, "Tipo de casilla: " + edificio, "Cuadro pulsado", JOptionPane.INFORMATION_MESSAGE);
	}
}