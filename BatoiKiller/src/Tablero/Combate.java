package Tablero;

import javax.swing.JFrame;

public class Combate {

	static JFrame ventana;
	
	public Combate() {
		
		ventana = new JFrame("COMBATE");
		VistaCombate combate = new VistaCombate();

		ventana.add(combate);
		ventana.setSize(815, 580);
		ventana.setVisible(true);
		ventana.setLocationRelativeTo(null);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		PantallaPrincipal.ventana.setVisible(false);
		
		
	}

	public JFrame getVentana() {
		return ventana;
	}

	public void setVentana(JFrame ventana) {
		this.ventana = ventana;
	}
	
	

}
